<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    .warning {
        background-color: orange;
    }
    </style>
</head>
<body>

    <form id="user">
        <input type="text" name="username">
        <input type="text" name="subject">
        <input type="email" name="email">
        <button>load</button>
    </form>

    <!-- <p class="errors"></p> -->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

  <script>
    //ajax

    $('#user').submit(function (e) {
        e.preventDefault();
            console.log(e);

        //duomenu surinkimas
        //serialize
        var data = {
            username: $(this).children('input[name=username]').val(),
            subject: $(this).children('input[name=subject]').val(),
            email: $(this).children('input[name=email]').val()
        };

        var that = $(this);

        console.log(data);

        $.ajax({
            url: 'data.php',
            method: "POST",
            data: data,
            dataType: 'JSON',
            success: function(response) {
                console.log(response)
                if (response.status === 'warning') {
                    // $('.errors').text(response.message);
                    // that.append('<p>Labas</p>'); - apacia
                    that.prepend(`<p class='warning'>${response.message}</p>`);
                }
            }
        })

    })



  </script>
</body>
</html>