<?php

    $response = [];

    if (!$_POST['username'] || !$_POST['subject'] || !$_POST['email']  ) {
        $response = [
            'status' => 'warning',
            'message' => 'empty field'
        ];
        echo json_encode($response);
        return;
    }

    $username = $_POST['username'];
    $subject = $_POST['subject'];
    $email = $_POST['email'];

    $to = 'robis159@gmail.com';
    $msg = $username . " want to connect with you customer email - " . $email;

    mail($to, $subject, $msg);

    $response = [
        'status' => 'success',
        'message' => 'email sent'
    ];
    echo json_encode($response);