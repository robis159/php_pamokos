<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Goods</title>
    <link rel="stylesheet" href="css/addForm.css">
</head>
<body>
    <div class="container">
        <div class="inputs">
            <div class="par">
                <p>Add goods: </p>
            </div>
            <input id="name" type="text" placeholder="Name of goods">
            <input id="quantity" type="number" placeholder="Quantity">
            <input id="price" type="number" placeholder="Price">
            <input id="discount" type="number" placeholder="Discount">
            <button id="button">check</button>
        </div>
        <div class="button">
            <a class="back_to_index" href="index.php">Back to home page</a>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <?php 
        require('js/addtoarr.php');
    ?>
  <script src="js/script.js"></script>
</body>
</html>