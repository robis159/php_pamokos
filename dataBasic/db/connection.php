<?php
    //prisijungimas prie duomenu bazes

    const HOST = "localhost";
    const DBNAME = "phppamokos";
    const USERNAME = "root";
    const PASSWORD = "";

    try {
        //bandymas prisijunkti prie domenu bazes
        // $connection = new PDO("mysql:host=SERVER;dbname=DATABASE", $username, $password);

        $connection = new PDO("mysql:host=" . HOST . ";dbname=" . DBNAME, USERNAME, PASSWORD);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        echo "connect";

    } catch (PDOException $e) {
        echo "ERROR: " . $e->getMessage();
    }

?>