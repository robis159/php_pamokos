<script>

function showElements() {

  //isitraukiam visus elementus kuriuos naudosim
  // var input, filter, table, tr, td, i;
  var input = document.getElementById("inputName");
  var inp_val = input.value;
  var table = document.getElementById("shopingTable");
  var tr = table.getElementsByTagName("tr");

  //sukam cikla ir tikrinam ar atitinka musu ieskoma reiksme
  for (i = 0; i < tr.length; i++) {
    //tirkinam musu paieska pagal eilutes antra stulpeli
    td = tr[i].getElementsByTagName("td")[1];
    //su salika tikrinam ar musu ivesta reiksme
    //yra ar ne, jaigu ne tada mes neatitinkancius paslepem
    if (td) {
      if (td.innerHTML.indexOf(inp_val) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>