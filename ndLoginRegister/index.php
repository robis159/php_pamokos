<?php
session_start();

//tikrina ar vartotojas prisijunges
if(!empty($_SESSION['logedin']))
{
    header("Location: lentele.php");
}

//prijungiam db
require('db/connection.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="css.cssReset.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/body.css">
    <link rel="stylesheet" href="css/loginregister.css">
</head>
<body>
    <div class="container">
        <div class="form-container">
            <?php require('forms/login.php') ?>
            <?php require('forms/register.php') ?>
        </div>
    </div>
</body>
</html>

