<?php
//isitraukem is sesijos user id
$user_id = $_SESSION['user_id'];

//prijungiam db
require("db/connection.php");

//istraukiami duomenys is db user product pagal user_id
$sql = "SELECT * FROM user_products WHERE user_id=" . $user_id;
$stm = $conn->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();

//sukuriam lenteliu eilutes pagal esancius vartotojo duomenis
for ($i=0; $i < count($result); $i++) {
    $produc_name = $result[$i]['name'];
    $produc_quantity = $result[$i]['quantity'];
    $produc_price = $result[$i]['price'];
    $produc_discount = $result[$i]['discount'];
    $produc_id = $result[$i]['id'];

    $id = $i + 1;
        echo "<form action='' method='GET'>";
            echo "<tr>";
                    echo "<td class='id'>".$id."</td>";
                    echo "<td class='name'>".$produc_name."</td>";
                    echo "<td class='link'> <a href=' ".str_replace(" ", "_", $produc_name).".php'>Link</a> </td>";
                    echo "<td class='quantity'>".$produc_quantity." vnt.</td>";
                    echo "<td class='price'>".$produc_price." &euro;</td>";
                    echo "<td class='discount'>".discountShow($produc_discount)."</td>";
                    echo "<td class='sum'>" . calculateSum($produc_quantity, $produc_price, $produc_discount) . "&euro;</td>";
                    echo "<td class='edit'> <a href='forms/editGoods.php?productId=".$produc_id."'>Edit</a> </td>";
                    echo "<td class='delete'> <a href='action/deleteUserSelectedGoods.php?productId=".$produc_id."'>Delete</a> </td>";
            echo "</tr>";
        echo "</form>";
};


$conn = null;

//apskaiciuoja lenteleje esanciu prekiu suma pagal kieki kaina ir nuolaida
function calculateSum($quantity, $price, $discount) {
    if ($discount === '0') {
        return $quantity * $price;
    } else {
        $sum = $quantity * $price;
        $procSum = ($sum / 100) * $discount;
        return $sum - $procSum;
    }
}

function discountShow($text) {
    if ($text === '0') {
        return "";
    } else {
        return "-" . $text . "%";
    }
}