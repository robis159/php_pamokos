<?php
session_start();

//tikriname ar vartotojas prisijunges
if(empty($_SESSION['logedin']))
{
    header("Location: ../index.php");
}

//prijungiam db
require("../db/connection.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css.cssReset.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/body.css">
    <link rel="stylesheet" href="../css/addGoods.css">
</head>
<body>
    <div class="container">
        <div class="form_container">
            <h1>Add product to the list</h1>
            <div class="inputs_names">
                <p>Produc name: </p>
                <p>Quantity of goods: </p>
                <p>Goods price: </p>
                <p>Discount goods: </p>
            </div>
            <form class="inputs_form" action="../action/userAddGoods.php" method="POST">
                <div class="goods_name">
                    <input id="inputName" class="form-control form-control-sm" type="text" name="name" onkeyup="showElements()">
                </div>
                <div class="goods_quantity">
                    <input class="form-control form-control-sm"  type="text" name="quantity">
                </div>
                <div class="goods_price">
                    <input class="form-control form-control-sm"  id="inputPrice" type="text" name="price">
                </div>
                <div class="goods_discount">
                    <input class="form-control form-control-sm"  type="text" name="dicount">
                </div>
                <button class="btn btn-outline-secondary">Add Goods</button>
            </form>
            <form class="existing_goods">
                <table class="table table-dark">
                    <thead>
                        <tr class="">
                            <th class="prod_id">Id</th>
                            <th class="prod_name">Product name</th>
                            <th class="prod_price">Product price</th>
                        </tr>
                    </thead>
                </table>
                <table class=" table table-sm table-hover" id="shopingTable">
                    <?php require('tableBodyAddPage.php'); ?>
                </table>
            </form>
        </div>
    </div>
    <?php
    require('../js/shoppingTableShowElem.php');
    ?>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="../js/shopingTableClickRow.js"></script>
</body>
</html>