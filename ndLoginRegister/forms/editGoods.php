<?php
session_start();
//tikriname ar vartotojas prisijungias
if(empty($_SESSION['logedin']))
{
    header("Location: ../index.php");
}

//prijungiam db
require("../db/connection.php");

// istraukiam duomenis pasirinkto produkto

$product_id = $_GET['productId'];
$sql = "SELECT * FROM user_products WHERE id=".$product_id;
$stm = $conn->prepare($sql);
$stm->execute();
$result = $stm->fetch();

$name = $result['name'];
$quantity = $result['quantity'];
$price = $result['price'];
$discount = $result['discount'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css.cssReset.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/body.css">
    <link rel="stylesheet" href="../css/editGoods.css">
</head>
<body>
    <div class="container">
        <div class="form_container">
            <h1 class="h1">Edit selected product</h1>
            <div class="inputs_names">
                <p>Produc name: </p>
                <p>Quantity of goods: </p>
                <p>Goods price: </p>
                <p>Discount goods: </p>
            </div>
            <form class="inputs_form" action="../action/editUserSelectedGoods.php?productId=<?php echo $product_id ?>" method="POST">
                <div class="goods_name">
                    <input class="form-control form-control-sm" type="text" name="name" value='<?php echo $name ?>'>
                </div>
                <div class="goods_quantity">
                    <input class="form-control form-control-sm" type="text" name="quantity" value='<?php echo $quantity ?>'>
                </div>
                <div class="goods_price">
                    <input class="form-control form-control-sm" type="text" name="price" value='<?php echo $price ?>'>
                </div>
                <div class="goods_discount">
                    <input class="form-control form-control-sm" type="text" name="discount" value='<?php echo $discount ?>'>
                </div>
                <button class="btn btn-outline-secondary">save changes</button>
            </form>
        </div>
    </div>
</body>
</html>
