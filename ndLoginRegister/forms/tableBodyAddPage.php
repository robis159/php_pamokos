<?php



$sql = "SELECT * FROM products";
$stm = $conn->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();

for($i = 0; $i < count($result); $i++) {
    $id = $i + 1;
    echo "<tr onclick='clickRow()'>";
        echo '<td class="prod_id">' . $id . '</td>';
        echo '<td id="tdName" class="prod_name">' . $result[$i]['name'] . '</td>';
        echo '<td id="tdprice" class="prod_price">' . $result[$i]['price'] . ' &euro;</td>';
    echo "</tr>";
}



$conn = null;