<?php
session_start();

if(empty($_SESSION['logedin']))
{
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lentele</title>
    <link rel="stylesheet" href="css.cssReset.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/body.css">
    <link rel="stylesheet" href="css/goodsTable.css">

</head>
<body>
    <div class="myContainer">
        <div class="table_container">
            <ul class="nav nav-pills ">
                <li class="nav-item">
                    <a class="nav-link active" href="action/userDisconect.php">Disconect</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="forms/addNewGoods.php">Add new items to the list</a>
                </li>
            </ul>
            <!-- <div class="disconect ">
                <a href="action/userDisconect.php" class="">Disconect</a>
            </div>
            <div class="add_goods">
                <a href="forms/addNewGoods.php">Add new items to the list</a>
            </div> -->
            <h1 class="h1">Shopping list</h1>
            <table class='table table-dark table-sm'>
                <tr>
                    <th class='id'> Id </th>
                    <th class='name'> Name </th>
                    <th class='link'> Link </th>
                    <th class='quantity'> Quantity </th>
                    <th class='price'> Vnt. price </th>
                    <th class='discount'> discount </th>
                    <th class='sum'> Total Sum </th>
                    <th class='edit'> edit </th>
                    <th class='delete'> delete </th>
                </tr>
            </table>
            <table class="bottom table table-sm table-hover table-active">
                <?php require('forms/shoppingTableBody.php'); ?>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</body>
</html>
