<?php
session_start();

if(!$_SESSION['loggedin'])
{
    header('Location: index.php');
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>lentele</title>
    <link rel="stylesheet" href="css/bodyreset.css">
    <link rel="stylesheet" href="css/lentele.css">
</head>
<body>

    <div class="container">
        <h1>Sveiki atvyke
            <?php
                echo $_SESSION['username'];
            ?>
        </h1>
        <div class="add_button">
            <a href="page_forms/add_form.php">Prideti prekes</a>
        </div>
        <div class="disconect_button">
            <form action="actions/disconect.php" method="post">
                <button class="button">Disconect</button>
            </form>
        </div>

        <h1 class='title'> Pirkiniu krepselis </h1>

        <table class='top'>
            <tr>
                <th class='id'> Id </th>
                <th class='name'> Name </th>
                <th class='link'> Link </th>
                <th class='quantity'> Quantity </th>
                <th class='price'> Vnt. price </th>
                <th class='discount'> discount </th>
                <th class='sum'> Total Sum </th>
                <th class='edit'> edit </th>
                <th class='delete'> delete </th>
            </tr>
        </table>

        <table class="body">
            <?php require('page_forms/table_body.php'); ?>
        </table>
    </div>

</body>
</html>
