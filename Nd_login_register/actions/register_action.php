<?php
    session_start();
    if (!empty($_SESSION['username'])) {
        session_destroy();
        session_start();
    }

    if (!empty($_POST['username'])) {
        $username =  $_POST['username'];
    }
    else {
        echo "The Username field needs to be filled up <br>";
    }

    if (!empty($_POST['password'])) {
        $password =  password_hash($_POST['password'], PASSWORD_BCRYPT);
    }
    else {
        echo "The Password field needs to be filled up <br>";
    }

    if(!empty($username) && !empty($password))
    {
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        $_SESSION['loggedin'] = false;
        var_dump($_SESSION);
        header("Location: ../index.php");
    }

