<?php
    //url visu faktu
    const BASE_URL = 'https://catfact.ninja/facts?limit=999';

try {
    //paleidziam curl
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, BASE_URL);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    //padarom kad butu galima istraukt info is saugomo http
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    //su json istraukeme visa info is array
    $result = json_decode(curl_exec($curl));
    // $randomResult = $result->data[0]->fact;
    //atspausdinam random facts
    // echo $result->data[0]->fact;
    echo json_encode($result->data[0]->fact);

} catch (PDOExeption $e) {
    echo "ERROR: " . $e->getMessage();
}