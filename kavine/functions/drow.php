<?php

    function drowCofeTable() {
        require("./../db/connectToDb.php");
        $sql = "SELECT * FROM cofe";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        // var_dump($result);
        $size = sizeof($result);
        for ($i = 0; $i < $size; $i++) {
            $id = $result[$i]['id'];
            echo '<tr onclick="clickRow()">';
                echo '<td style="display: none">' . $id . '</td>';
                echo "<td>" . $result[$i]['name'] . "</td>";
                echo "<td>" . $result[$i]['address'] . "</td>";
                echo "<td>" . $result[$i]['phonenumber'] . "</td>";
                echo "<td> <a>Edit</a> </td>";
                echo '<td> <a href="./../actions/deleteCofe.php?id=' . $id . '">Delete</a> </td>';
                echo "<td style='text-align: center'> <a href='../lankytouSugeneravimas.php?id=".$id."'>Add</a> </td>";
            echo '</tr>';
        }
    }

    function drowCaffeeList($cofeName) {
        include('./../db/connectToDb.php');
        //issitraukiam visus duomenis apie kavine
        $sqlCofe = "SELECT * FROM cofe WHERE name='$cofeName'";
        $stmtCofe = $conn->prepare($sqlCofe);
        $stmtCofe->execute();
        $resultCofe = $stmtCofe->fetch();
        $cofeId = $resultCofe['id'];
        $sqlall = "SELECT type.name, type.beens, type.price, caffeetype.type_name
        FROM ((jointypes INNER JOIN type ON jointypes.typeid=type.id) INNER JOIN caffeetype ON jointypes.caffetypeid=caffeetype.id)
        WHERE jointypes.cofeid='$cofeId'";

        $stmAll = $conn->prepare($sqlall);
        $stmAll->execute();
        $resultAll = $stmAll->fetchAll();

        for($i = 0; $i < sizeof($resultAll); $i++) {
            $listId = $i +1;
            $name = $resultAll[$i]['name'];
            $beens = $resultAll[$i]['beens'];
            $type = $resultAll[$i]['type_name'];
            $price = $resultAll[$i]['price'];
            $cofeName = $_GET['cofeName'];
            echo '<tr >';
                echo '<td scope="row">'.$listId.'</td>';
                echo '<td scope="row">'.$name.'</td>';
                echo '<td scope="row">'.$beens.'</td>';
                echo '<td scope="row">'.$type.'</td>';
                echo '<td scope="row">'.$price.'</td>';
                echo '<td scope="row"> <a href="./../pages/editCoffeeItem.php?name='.$name.'&beens='.$beens.'&type='.$type.'&price='.$price.'&cofeName='.$cofeName.'" >Edit</a> </td>';
                echo '<td scope="row"> <a href="./../actions/deleteCoffeeFromList.php?name='.$name.'&cofeName='.$cofeName.'">Delete</a> </td>';
            echo '</tr>';
        }

        $conn = null;
    }
