<?php
    include('./../db/connectToDb.php');
    $name = $_GET['name'];
    $beens = $_GET['beens'];
    $type = $_GET['type'];
    $price = $_GET['price'];
    $cofeName = $_GET['cofeName'];

    $sqlType = "SELECT * FROM type WHERE name='$name'";
    $stmtType = $conn->prepare($sqlType);
    $stmtType->execute();
    $resultType = $stmtType->fetch();
    $typeId = $resultType['id'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="./../css/editCoffee.css">
</head>

<body>
    <form action="./../actions/editcoffee.php" method="POST" class="form">
    <input  type="hidden" name="id" value="<?php echo $typeId; ?>">
    <input  type="hidden" name="cofeName" value="<?php echo $cofeName; ?>">
        <div class="elem">
            <div class="text">Coffee name</div>
            <input type="text" name="name" value="<?php echo $name; ?>">
        </div>
        <div class="elem">
            <div class="text">Been Type</div>
            <input type="text" name="beens" value="<?php echo $beens; ?>">
        </div>
        <div class="elem">
            <div class="text">Coffee type</div>
            <input type="text" name="type" value="<?php echo $type; ?>">
        </div>
        <div class="elem">
            <div class="text">Price</div>
            <input type="text" name="price" value="<?php echo $price; ?>">
        </div>
        <div class="button">
            <button>Save</button>
        </div>
    </form>
</body>

</html>