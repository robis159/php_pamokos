<?php
// pasiimam kavines pavadinima ir id is js perduoto per GET metoda
$name = $_GET['cofeName'];
$cofeId = $_GET['cofeId'];
//is db isitraukiame visa info apie ta kavine
include('./../pageLogic/getElements.php');
include('./../actions/getCustomers.php');
$result = getCofeList('cofe', $name);
$customersArray = getCustomers($cofeId);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="viewport" content="initial-scale=1.0,
    width=device-width" />
    <script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
    <script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">

    <link rel="stylesheet" href="./../css/cofe.css">
</head>

<body>
    <h1 class="cofe_name"> Welcome to the
        <?php echo $name ?>
    </h1>
    <div class="form_container">
        <div class="left">
            <div class="cofe_info">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        About Cofe
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Name:
                        <?php echo $result['name'] ?>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Address:
                        <?php echo $result['address']; ?>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Phone number:
                        <?php echo $result['phonenumber'] ?>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Number of visitors:
                        <?php echo sizeof($customersArray); ?>
                    </a>
                </div>
                <div style="width: 300px; height: 225px" id="mapContainer"></div>
            </div>
            <?php
        session_start();
        if ($_SESSION['permision'] === '1')
        {
            ?>
            <form class="addForm" action="./../actions/caffeeAdd.php?name=<?php echo $name ?>" method="POST">
                <h1>Add a new caffee</h1>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Coffee name</span>
                    </div>
                    <input name="name" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Been type</span>
                    </div>
                    <input name="been" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Coffee type</span>
                    </div>
                    <input name="type" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Price</span>
                    </div>
                    <input name="price" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="button_div">
                    <button class="add_button btn btn-outline-secondary">Save</button>
                </div>
            </form>
            <?php
        }
    ?>
        </div>

        <div class="right">
            <h3>coffee list in this cafe</h3>
            <div class="info_container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Coffee name</th>
                            <th scope="col">Bean type</th>
                            <th scope="col">Coffee type</th>
                            <th scope="col">Price</th>
                            <?php
                            if ($_SESSION['permision'] === '1') {
                                echo '<th scope="col">Edit</th>';
                                echo '<th scope="col">Delete</th>';
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                         include('./../functions/drow.php');
                         drowCaffeeList($name);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
  require('../js/googleMapApi.php');
  ?>

</body>

</html>