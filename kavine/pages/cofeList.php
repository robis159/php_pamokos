<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cofe List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="./../css/cofelist.css">
</head>

<body>
    <?php
        session_start();
        if($_SESSION['permision'] === '1')
        {
            ?>
            <form class="addForm" action="../actions/newCofeAdd.php" method="POST">
                <h1>Add a new cafe</h1>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input name="name" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Adsress</span>
                    </div>
                    <input name="adres" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend name_width">
                        <span class="input-group-text name_width" id="inputGroup-sizing-default">Phone number</span>
                    </div>
                    <input name="phoneNumber" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="button_div">
                    <button class="add_button btn btn-outline-secondary">Save</button>
                </div>
            </form>
            <?php
        }
    ?>

    <div class="table_div">
        <h1 class="h1_table">Cofe list in Klaipėda</h1>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Cofe Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone Number</th>
                    <?php
                    if($_SESSION['permision'] === '1') {
                        echo '<th scope="col">Edit</th>';
                        echo '<th scope="col">Delete</th>';
                        echo '<th scope="col">Add Customers</th>';
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
               <?php
                    require('./../functions/drow.php');
                    drowCofeTable();
               ?>
            </tbody>
        </table>
    </div>

    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <!-- <script src="../js/cofeList.php"></script> -->
  <?php
  require('../js/cofeList.php');
  ?>
</body>

</html>

<?php

if (isset($_SESSION['alert'])) {
    if ($_SESSION['alert']) {
        echo '<script language="javascript">';
        echo 'alert("Add 100 customers.")';
        echo '</script>';
    }
    unset($_SESSION['alert']);
}