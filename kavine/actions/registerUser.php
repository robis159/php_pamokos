<?php
require('./../db/connectToDb.php');


if (!empty($_POST['username'])) {
    $username = $_POST['username'];
} else {
    header('Location: ../index.php');
    return;
}

if (!empty($_POST['password'])) {
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
} else {
    header('Location: ../index.php');
    return;
}

while (true) {
    $unicid = getToken(10);
    // $unicid = '2Q2GYCGYie';

    $sql = 'SELECT COUNT(*) FROM users WHERE unicid = ?';
    //$sql = 'SELECT COUNT(*) from table WHERE param = ?'; // for checking >1 records
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $unicid, PDO::PARAM_INT);
    $stmt->execute();

    if(!$stmt->fetchColumn()){
        if (!empty($username) && !empty($password)) {
            try {
                $sql = "INSERT INTO users (username, password, unicid) VALUES ('".$username."', '".$password."', '".$unicid."')";
            $conn->exec($sql);
            } catch (PDOExepsion $e) {
                echo "ERROR: " . $e->getMessage();
            }
        }
        break;
    } else {
        continue;
    }
}

header("Location: ../index.php");

function getToken($length){
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

   for ($i=0; $i < $length; $i++) {
       $token .= $codeAlphabet[random_int(0, $max-1)];
   }

   return $token;
}