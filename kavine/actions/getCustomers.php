<?php

function getCustomers($cofeId) {
    try {
        include('./../db/connectToDb.php');
    $sql = "SELECT customers.unicid, customers.come_time, customers.leave_time
    FROM customers_come INNER JOIN customers ON customers_come.customersid=customers.id
    WHERE customers_come.cofeid='$cofeId'";

    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    return $result;
    } catch (PDOExeption $e) {
        $message = $e->getMessage();
        echo '<script language="javascript">';
        echo 'alert("'.$message.'")';
        echo '</script>';
    }
}
