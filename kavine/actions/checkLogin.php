<?php
require('../db/connectToDb.php');
session_start();

if (!empty($_POST['username'])) {
    $username = $_POST['username'];
} else {
    header('Location: ../index.php');
    return;
}

if (!empty($_POST['password'])) {
    $password = $_POST['password'];
} else {
    header('Location: ../index.php');
    return;
}

if (!empty($username) && !empty($password)) {
    try {
        $sql = "SELECT * FROM users WHERE username='$username'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $user = $stmt->fetch();
    } catch (PDOExepsion $e) {
        echo "ERROR: " . $e->getMessage();
    }

    if ($username !== $user['username']) {
        echo "the user does't exist";
        return;
    }

    if (!password_verify($password, $user['password']))
    {
        echo "password is incorrect";
        return;
    }
    $_SESSION['status'] = "connected";
    $_SESSION['permision'] = $user['permision'];
    $_SESSION['username'] = $user['username'];


    echo "Prisijungimas pavyko";
    header("Location: ../pages/cofeList.php");
}