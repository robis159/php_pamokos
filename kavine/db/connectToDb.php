<?php

// prisijungimo duomenys
$host = 'localhost';
$dbName = 'kavine';
$username = 'root';
$password = '';

//prisijungiame prie duomenu bazes
try {
    $conn = new PDO("mysql:host=" . $host . ";dbname=" . $dbName, $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo "Connected <br>";
} catch (PDOException $e) {
    echo "ERROR: " . $e->getMessage();
}