<?php
const HOST = "localhost";
const DBNAME = "cars";
const USERNAME = "root";
const PASSWORD = "";

try {
    //bandymas prisijunkti prie domenu bazes
    // $connection = new PDO("mysql:host=SERVER;dbname=DATABASE", $username, $password);

    $conn = new PDO("mysql:host=" . HOST . ";dbname=" . DBNAME, USERNAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // echo "connected";

} catch (PDOException $e) {
    echo "ERROR: " . $e->getMessage();
}