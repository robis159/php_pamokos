<?php
    //funkcija kuri sukure lentele prisijungtoje duomenu bazeje
    function createTable($name, $conn, $dbname) {
        //iskvieciam funkcija kuri patikrina ar lentele egzistuoja
        $chechValue = checkTable($name, $conn, $dbname);

        //jaigu patikrinimo rezultatas 0 tada sukureme lentele duomenu bazeje
        if(!$chechValue) {
            try {
                // sukureme lentele daudodami sql komanda
                $sql = "CREATE TABLE $name (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    vardas VARCHAR(30),
                    amzius INT(10),
                    greitis FLOAT(10)
                )";
                $conn->exec($sql);
                    //prie lenteles iskarto pridedami 5 sportininkai
                    instertToTable('Tadas', 19, 9.8, 'sportininkai', $conn);
                    instertToTable('Zidrunas', 23, 6.7, 'sportininkai', $conn);
                    instertToTable('Jonas', 18, 11.3, 'sportininkai', $conn);
                    instertToTable('Antanas', 27, 11.1, 'sportininkai', $conn);
                    instertToTable('Petras', 25, 7.9, 'sportininkai', $conn);

                echo "Table $name created successfully";
            } catch(PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
        }
    }

    // funkcija kuri patikrina ar lentele egzistuoja
    // 0 - lentele neegzistuoja
    // 1 - lentele egzistuoja
    function checkTable($name, $conn, $dbname) {
        $checkTable = $conn->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='$dbname'");
        return $checkTable->fetchColumn();
    }
    //funkcija yra skkirta iterpti reiksmes i lentele
    function instertToTable($vardas, $amzius, $greitis ,$tableName, $conn) {
        try {
            $sql = "INSERT INTO $tableName (vardas, amzius, greitis) VALUES ('".$vardas."', $amzius, $greitis)";
            if ($conn->exec($sql)) {
                echo "Yrasas irasytas";
            } else {
                echo "nerirasytas";
            }
        } catch (PDOExeption $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $tableName
     * @param [type] $conn
     * @return JsonResult
     */
    function selectFromTable($tableName = null, $conn = null) : JsonResult {
        if  (!$tableName)
            return;
        if (!$conn)
            return;

        $sql = "SELECT * FROM $tableName ORDER BY greitis DESC";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
        return json_encode($result);
    }
?>