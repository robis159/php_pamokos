<?php
// sukureme prisijungimo duomenu konstantas
const HOST = "localhost";
const DBNAME = "turnyras";
const USERNAME = "root";
const PASSWORD = "";

//bandome prisijunkti prie phpmyadmin duomenu bazes pavadinimu DBNAME
try {
    $conn = new PDO("mysql:host=" . HOST . ";dbname=" . DBNAME, USERNAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected <br>";
} catch (PDOExeption $e) {
 echo "ERROR: " . $e->getMessage();
}

?>