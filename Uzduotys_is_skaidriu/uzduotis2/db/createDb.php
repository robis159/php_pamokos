<?php
// sukureme prisijungimo duomenu konstantas
const HOST1 = "localhost";
const USERNAME1 = "root";
const PASSWORD1 = "";
$dbName = "turnyras";

//prisijungiame ir sukuriame duomenu baze
try {
    $conn = new PDO("mysql:host=" . HOST1, USERNAME1, PASSWORD1);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->query("SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$dbName'");
        if (!$stmt->fetchColumn()) {
            $sql = "CREATE DATABASE $dbName charset=utf8";
            $conn->exec($sql);
            echo "Database created successfully<br>";
        }
    }
catch(PDOException $e)
    {
        echo $e->getMessage();
    }

$conn = null;

?>