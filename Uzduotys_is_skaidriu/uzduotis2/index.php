<?php
    //tikrina ar yra sukurta duomenu baze, jaigu ne tai ja sukure
    require('db/createDb.php');

    //tikrina ar duomenu lentele egzistuoja jai ne ja sukure
    require('./db/connectToDb.php');
    require('./db/createTable.php');
    createTable('sportininkai', $conn, DBNAME);
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form id="sportininkas">
        <input type="text" placeholder="Vardas" name="vardas">
        <input type="number" placeholder="Amzius" name="amzius">
        <input type="Number" step="0.01" placeholder="Greitis" name="greitis">
        <button>Prideti</button>
    </form>
    <div class="testas">
        <p style="color: red">Rikevimas nuo greiciausio iki leciausio</p>
        <?php
            //isitraukiame visa info is sportininku lenteles
            $result = selectFromTable('sportininkai', $conn);
            $size = 0;
            // nustatome lenteled dydi kury naudosime veliau
            if ($result){
                $size = sizeof(json_decode($result));
                //isikvieciam funkcia kuri suves i weba info apie sportininkus
                require('./actions/writeinfo.php');
                showAll($result, 0);
            }
        ?>
        <p style="color: red">top 3 sportininkai</p>
        <?php
        //isikvieciam funkcia kuri suves i weba info apie sportininkus
        //taciau dabar ciklas suksis kol pasieks pirmus 3 sportininkus
        $result ? showAll($result, 3):'';
        ?>
        <p style="color: red">isviso dalyviu</p>
        <?php
            //naudoja auksciau sugeneruota lenteles dydzio info ir isveda i weba
            echo "Isviso dalyviu yra: " . $size ?: 0;
        ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="./js/script.js"></script>
</body>
</html>
<?php
$conn = null;