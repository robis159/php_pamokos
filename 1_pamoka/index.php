<?php
// php kodas
$kintamasis = "labas";

// echo "Poirma pamoka <br>";
// echo $kintamasis;

// string interpolation
// echo "$kintamasis - tekstas"; // ispausdins kintamojo reiksme + teksta sale
// echo '$kintamasis - tekstas'; // kintamaji ispausdins kaip teksta 

//int
//string
//booolean
//float
//arrays
//object

//mastyvas

// $arr = [1, 2, 3];
// $arr = array(1, 2, 3);

// phpinfo(); viskas apie php
// echo phpversion(); ispausdina tik php versija

//array ispausdinimui
// print_r($arr);
// var_dump($arr);

// for ($i = 0; $i < 3; $i++)
// {
//  echo $arr[$i];
// }

// foreach($arr as $a) {
//     echo $a;
// }

// pavadinimas();

// function pavadinimas() {
//     echo date('Y-m-d');
// }

// //konstantos

// define('NAME', 'Robertas');
// const _NAME = 'Lekavicius';
// echo NAME;
// echo _NAME;
//objektai

class Car {
    // function Car() {
    //     $this->brand = 'Audi';
    // }
        public $brand; 
        public $color;
        public $buy_date;
        public $age;

    public function __construct($brand, $color = 'white', $buy_date) {
        $this->brand = $brand;
        $this->color = $color;

        $this->buy_date = $buy_date;
        $this->age = $this->calculateAge();
    }

    private function calculateAge() {
        return 2018 - $this->buy_date;
    }
}

$audi = new Car('Audi', 'white', 1990);

echo $audi->brand;
echo $audi->color;
echo $audi->age;
echo $audi->buy_date;


// kad pasiekt sekanti faile paciame failo virsuje irasome
 require('footer.php');
// require_once('antras.php');
// include('antras.php');
// include_once('antras.php');

// echo $labas;


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php 
        $x = 10;
        
        // if ($x === 10)
        // {
        //     echo "<h1>labas</h1>";
        // }
        if ($x === 10)
        {
            ?>

            <h1>
                Labas Kebabas
            </h1>
            <h1>
            </h1>
            
            <?php
        }

        $array = [1, 2, 3, 4, 5];

        foreach($array as $rr): ?> 
            <h1> 
                <?php echo $rr; ?>
            </h1>
        <?php
        endforeach;
    ?>

</body>
</html>

