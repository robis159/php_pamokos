<?php
function DrowTableBody($arr) {
    $length = sizeof($arr);
    echo "<table class='body'>";
        for($i = 0; $i < $length; $i++)
        {
            echo "<tr>";
                $id = $i +1;
                echo "<th class='id'>".$id."</th>";           
                echo "<th class='name_body'>".$arr[$i]['name']."</th>";           
                echo "<th class='link' > <a href='#'>Link</a> </th>";           
                echo "<th class='quantity'>".$arr[$i]['quantity']."</th>";           
                echo "<th class='price'>".$arr[$i]['price']." &euro;</th>"; 
                echo Discount($arr, $i);           
                echo "<th class='sum'>".CalculateSum($arr, $i)." &euro;</th>";           
            echo "<tr>";
        }
    echo "</table>";
}

function CalculateSum($arr, $i) {
    $discount = $arr[$i]['nuolaida'];
    if ($discount === " ")
    {
        return $arr[$i]['quantity'] * $arr[$i]['price'];
    }
    else
    {
        $fullsum = $arr[$i]['quantity'] * $arr[$i]['price'];
        $discountsum = ($fullsum / 100) * $discount;
        return $fullsum - $discount;
    }
}

function Discount($arr, $i) {
    $discount = $arr[$i]['nuolaida'];
    if ($discount === " ")
    {
        return "<th class='discount'>".$arr[$i]['nuolaida']."</th>";
    }
    else
    {
        return "<th class='discount'>-".$arr[$i]['nuolaida']."%</th>";
    }
}

?>