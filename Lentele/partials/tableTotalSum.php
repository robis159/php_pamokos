<?php
    function DrowTotalSum($arr) {
        echo "<p class='total_sum'> Total sum: ".CalculateTotalSum($arr)." &euro;</p>";
    }


function CalculateTotalSum($arr) {
    $totalsum = 0;

    for ($i=0; $i < sizeof($arr); $i++) { 
        
        $discount = $arr[$i]['nuolaida'];
       
        if ($discount === " ")
        {
            $totalsum += $arr[$i]['quantity'] * $arr[$i]['price'];
        }
        else
        {
            $fullsum = $arr[$i]['quantity'] * $arr[$i]['price'];
            $discountsum = ($fullsum / 100) * $discount;
            $totalsum += $fullsum - $discount;
        }
    }

    return $totalsum;
}

?>