<?php

$prekes_arr = [
    [   
        'id' => 1,
        'name' => "Kilimelis mankstai",
        'link' => "kilimelis_mankstai.php",
        'nuolaida' => 15,
        'quantity' => 10,
        'price' => 29.95
    ],
    [
        'id' => 2,
        'name' => "Gimnastikos kamuolys",
        'link' => "gimnastikos_kamuolys.php",
        'nuolaida' => " ",
        'quantity' => 18,
        'price' => 12.95
    ],
    [
        'id' => 3,
        'name' => "Lapu greblys",
        'link' => "lapu_greblys.php",
        'nuolaida' => " ",
        'quantity' => 8,
        'price' => 13.98
    ],
    [
        'id' => 4,
        'name' => "Sodo irankiu deze",
        'link' => "Sodo_rankiu_deze.php",
        'nuolaida' => 13,
        'quantity' => 1,
        'price' => 58.38
    ],
    [
        'id' => 5,
        'name' => "Oro kondicionierius",
        'link' => "oro_kondicionierius.php",
        'nuolaida' => 26,
        'quantity' => 1,
        'price' => 1286.01
    ],
    [
        'id' => 6,
        'name' => "Automobilio padangos",
        'link' => "Automobilio_padangos.php",
        'nuolaida' => " ",
        'quantity' => 4,
        'price' => 118.11
    ],
    [
        'id' => 7,
        'name' => "Variklio alyva GM DEXOS2 5W30, 5L",
        'link' => "variklio_alyva.php",
        'nuolaida' => " ",
        'quantity' => 1,
        'price' => 20.99
    ],
    [
        'id' => 8,
        'name' => "Luminarc Lotusia pietų servizas, 30 dalių",
        'link' => "pietu_servizas.php",
        'nuolaida' => 10,
        'quantity' => 2,
        'price' => 49.50
    ],
    [
        'id' => 9,
        'name' => "Indelis džemui su dangteliu, 9 cm",
        'link' => "indas_dzemui.php",
        'nuolaida' => " ",
        'quantity' => 3,
        'price' => 2.16
    ],
];

?>